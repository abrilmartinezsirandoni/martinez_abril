<?php

if($_POST['product-form'] == 'add'){
    $name = $_POST['pname'];
    $precio = $_POST['pprice'];
    $code = $_POST['pcode'];
    $directorio = "img/";


    if(!is_dir($directorio)){
        mkdir($directorio, 0755, true);
    }
    if ($_FILES['avatar']['type']=='image/png') {
        $ext='.png';
    } elseif ($_FILES['avatar']['type']=='image/jpeg') {
        $ext='.jpeg';
    } elseif ($_FILES['avatar']['type']=='image/jpg') {
        $ext='.jpg';
    } elseif ($_FILES['avatar']['type']=='image/gif') {
        $ext='.gif';
    }
    if(move_uploaded_file($_FILES['avatar']['tmp_name'], $directorio . $name . $ext)) {
        $avatar_url = $name.$ext;
        $programa_resultado = "Se subió correctamente";
    } else {
        $respuesta = array(
            'respuesta' => error_get_last()
        );
    }


    try{
        include_once 'config/db.php';
        $stmt = $conn->prepare("INSERT INTO products (product_name, product_price, product_image, product_code) VALUES (?, ?, ?, ?)");
        $stmt->bind_param('ssss', $name, $precio, $avatar_url, $code);
        $stmt->execute();

        $id_insertado = $stmt->insert_id;
        $errno = $stmt->errno;

        if($stmt->affected_rows && $errno === 0){
            $respuesta = array(
                'respuesta' => 'exitoso',
                'id' => $id_insertado
            );
        } elseif($errno === 1406) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        } elseif($errno === 1062) {
            $respuesta = array(
                'respuesta' => 'error',
                'errno' => $errno,
                'error' => $stmt->error,
            );
        }
        $stmt->close();
        $conn->close();




    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }
    die(json_encode($respuesta));
} elseif ($_POST["product-form"]== "edit") {
    $id = $_POST['id_product'];
    $name = $_POST['name'];


    if (!empty($_POST['name'])) {
        $name=$_POST['name'];
    
        try{
            include_once 'config/db.php';
            $stmt = $conn->prepare("UPDATE products SET product_price = ? WHERE id= ?;");
            $stmt->bind_param ('si',$name, $id);
            $stmt->execute();

            $id_insertado = $stmt->insert_id;
            $errno = $stmt->errno;
            
            if($stmt->affected_rows && $errno === 0) {
                $respuesta = array(
                    "respuesta" => "exitoso");
                    
            } elseif($errno === 1406) {
                $respuesta = array(
                    "respuesta" => "error",
                    "errno"=> $errno,
                    "error"=> $stmt->error,
                );
            } elseif($errno === 1062) {
                $respuesta = array(
                    "respuesta" => "error",
                    "errno"=> $errno,
                    "error"=> $stmt->error,
                );
            }     
            $stmt->close();
            $conn->close();
        } catch (Exception $e) {
            echo "Error: " , $e->getMessage();
        }die(json_encode($respuesta));
    }
}    
if(isset($_GET['remove'])){
    include_once 'config/db.php';
    $id= $_GET['remove'];
    $stmt= $conn->prepare("DELETE FROM products WHERE id=?");
    $stmt-> bind_param("i", $id);
    $stmt->execute();

    $_SESSION['showAlert']= 'block';
    $_SESSION['message']= 'Se eliminó el producto del carrito!';
    header('location:list-modif-product.php');
}