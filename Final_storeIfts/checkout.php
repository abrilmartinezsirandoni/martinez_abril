<?php
	include_once 'config/db.php';

	$grand_total = 0;
	$allItems = '';
	$items = [];

	$sql = "SELECT CONCAT(product_name, '(',qty,')') AS ItemQty, total_price FROM cart";
	$stmt = $conn->prepare($sql);
	$stmt->execute();
	$result = $stmt->get_result();
	while ($row = $result->fetch_assoc()) {
	  $grand_total += $row['total_price'];
	  $items[] = $row['ItemQty'];
	}
	$allItems = implode(', ', $items);
?>

<?php
include "header.php";
?>

 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Ribbons</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Ribbons</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    
    <section class="content">
        <div class="container-fluid">
            <div class="row" id="order">
              <div class="col-md-12">
                <div class="card">
                <h4 class="text-center  text-info p-2"></h4>
                <div class=" p-3 mb-2 text-center">
                    <h6 class="lead"><b>Product(s) : </b><?= $allItems; ?></h6>
                    <h6 class="lead"><b>Envio : </b>Free</h6>
                    <h5><b>Total: </b><?= number_format($grand_total,2) ?></h5>
                </div>
                <form action="" method="post" id="orden">
                  <div class="row">
                    <div class="col-md-12">
                    <input type="hidden" name="products" value="<?= $allItems; ?>">
                    <input type="hidden" name="grand_total" value="<?= $grand_total; ?>">
                    <div class="form-group">
                        <input type="text" name="name" class="form-control" placeholder="ingrese nombre" required>
                    </div>
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="ingrese email" required>
                    </div>
                    <div class="form-group">
                        <input type="tel" name="phone" class="form-control" placeholder="ingrese telefono" required>
                    </div>
                    <div class="form-group">
                        <textarea name="address" class="form-control" rows="3" cols="10" placeholder="Direccion de entrega"></textarea>
                    </div>
                    <h6 class="text-center lead">Select Payment Mode</h6>
                    <div class="form-group">
                        <select name="pmode" class="form-control">
                        <option value="" selected disabled>Seleccione forma de pago</option>
                        <option value="cod">Mercado Pago</option>
                        <option value="netbanking">Transferencia</option>
                        <option value="cards">Debit/Credit Card</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="submit" value="Realizar pedido" class="btn btn-primary btn-block">
                    </div>

                    </div>

                  </div>
                </form>

                </div> <!-- card -->


              </div>

            </div> <!-- class row -->
        </div> <!-- container ---> 
    </section>
</div > <!-- class="content-wrapper"--> 


<?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  echo $file;
  include "scripts/script-$file";
?> 
