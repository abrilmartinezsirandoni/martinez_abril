<?php

if(isset($_POST['login-form'])) {
    $usuario= $_POST['email']; //tener datos del campo en el archivo
    $password= $_POST['pass'];

    try{
        include_once "config/db.php";
        $dat = $conn->prepare("SELECT * FROM user WHERE user_email= ?;");      
        $dat->bind_param('s', $usuario); //dindeo de param, se especifica con este metodo el param
        $dat->execute();
        $dat-> bind_result($id,$user_name, $user_email, $pass ,$dateCreate, $dateUpdate);
        if($dat->affected_rows){
            $existe= $dat->fetch(); //para trabajar con un dato a la array
            if ($existe){             //si el usuario existe ejecuta este pedazo de codigo 
                if ($password == $pass){   
                    $respuesta= array(
                        'respuesta'=> 'exitoso',  //array asociativo 
                        'usuario'=> $user_name,
                    );
                    
                }else{
                    $respuesta= array(
                        'respuesta'=> 'error_pass',
                    );

                }
            }else{     //si el usuario no existe 
                $respuesta= array(
                    'respuesta'=> 'error',
                    'email'=> $usuario
                );
            }
        }  
        //dsp de dindear el param, se ejecutra 
        //en el signo ? viaja usuarios pero se pasa en la prox linea para especifiar type 


    } catch (Exception $e){
        echo "Error: ", $e->getMessage();
        
    }
    die(json_encode($respuesta));

}

?>