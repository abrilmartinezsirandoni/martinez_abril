

function calculation(){
    var num1 = parseFloat(document.getElementById('num1').value);
    var numOp = parseInt(document.getElementById('numOp').value);
    var num2 = parseFloat(document.getElementById('num2').value);
    var modal = document.getElementById("modal-body");
    switch(numOp){
        case 1:
            var res = num1+num2;
            var ops = "Suma";
            break;
        case 2:
            var res = num1-num2;
            var ops = "Resta";
            break;
        case 3:
            var res = num1*num2;
            var ops = "Multiplicación";
            break;
        case 4:
            var res = (num1/num2).toFixed(2);
            var ops = "División";
            break;
    }

    modal.innerHTML = ` 
    El primer numero es:  <div class='fw-bold'>${num1} </div> el operador es:  <div class='fw-bold'> ${numOp} ${ops} </div> El segundo numer es: <div class='fw-bold'> ${num2} </div> El resultado es <h2 class='display-6 text-center fw-bold'>${res}</h2>
    `;
}

function text(){
    var num1 = parseFloat(document.getElementById('num1').value);
    var numOp = parseInt(document.getElementById('numOp').value);
    var num2 = parseFloat(document.getElementById('num2').value);
    var modal = document.getElementById("modal-body");
    if(numOp == 1){
        var res = num1+num2;
        var ops = "Suma";

        //guardar en la tabla
        var fila="<tr><td>"+num1+"</td><td>"+num2 +"</td><td>"+res +"</td></tr>";
        var btn = document.createElement("TR");
        btn.innerHTML=fila;
        document.getElementById("tbody").appendChild(btn);

    }
    if(numOp == 2){
        var res = num1-num2;
        var ops = "Resta";


        //guardar en la tabla
        var fila="<tr><td>"+num1+"</td><td>"+num2 +"</td><td>"+res +"</td></tr>";
        var btn = document.createElement("TR");
        btn.innerHTML=fila;
        document.getElementById("tbody").appendChild(btn);
    }

    if(numOp == 3){
        var res = num1*num2;
        var ops = "Multiplicación";

        //guardar en la tabla
        var fila="<tr><td>"+num1+"</td><td>"+num2 +"</td><td>"+res +"</td></tr>";
        var btn = document.createElement("TR");
        btn.innerHTML=fila;
        document.getElementById("tbody").appendChild(btn);
    }

    if(numOp == 4){
        var res = num1/num2;
        var ops = "División";

        //guardar en la tabla
        var fila="<tr><td>"+num1+"</td><td>"+num2 +"</td><td>"+res +"</td></tr>";
        var btn = document.createElement("TR");
        btn.innerHTML=fila;
        document.getElementById("tbody").appendChild(btn);
    }

    modal.innerHTML = ` 
    El primer numero es:  <div class='fw-bold'>${num1} </div> el operador es:  <div class='fw-bold'> ${numOp} ${ops} </div> El segundo numer es: <div class='fw-bold'> ${num2} </div> El resultado es <h2 class='display-6 text-center fw-bold'>${res}</h2>
    `;
//    for(var i=0; i<10;i++){
//        alert("num:" + i);
//    }

}

/*
function guardar(){
   
    var num1 = parseFloat(document.getElementById('num1').value);
    var numOp = parseInt(document.getElementById('numOp').value);
    var num2 = parseFloat(document.getElementById('num2').value);

    var fila="<tr><td>"+num1+"</td><td>"+num2 +"</td><td>"+res +"</td></tr>";

    var btn = document.createElement("TR");
   	btn.innerHTML=fila;
    document.getElementById("tbody").appendChild(btn);
}*/