<?php
    include "header.php";
?>
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Store IFTS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Store IFTS</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      <!-- Displaying Products Start -->
  <section class="content">
    <div id="message"></div>
    <div class="card card-solid">
      <div class="row">
        <?php
          try {
          include_once 'config/db.php';
          $stmt = ('SELECT * FROM products');
          $resultado = $conn->query($stmt);
          } catch (Exception $e) {
            $error =$e->getMessage();
            echo $error;
          }
          while ($row = $resultado->fetch_assoc()){
        ?>
        <div class="col-3 mr-5 ml-4">
          <div class="card-deck">
            <div class="card p-2 border-secondary mb-2">
              <img src="img/<?= $row['product_image'] ?>" class="card-img-top" height="250">
              <div class="card-body p-1">
                <h4 class="card-title text-center text-info"><?= $row['product_name'] ?></h4>
                <h5 class="card-text text-center text-danger">$<?= number_format($row['product_price'],2) ?></h5>

              </div>
              <div class="card-footer p-1">
                <form action="" class="form-submit">
                  <div class="row p-2">
                    <div class="col-md-6 py-1 pl-4">
                      <b>Cantidad: </b>
                    </div>
                    <div class="col-md-6">
                      <input type="number" class="form-control pqty" value="<?= $row['product_qty'] ?>">
                    </div>
                  </div>
                  <input type="hidden" class="pid" value="<?= $row['id'] ?>">
                  <input type="hidden" class="pname" value="<?= $row['product_name'] ?>">
                  <input type="hidden" class="pprice" value="<?= $row['product_price'] ?>">
                  <input type="hidden" class="pimage" value="<?= $row['product_image'] ?>">
                  <input type="hidden" class="pcode" value="<?= $row['product_code'] ?>">
                  <button class="btn btn-info btn-block addItemBtn"><i class="fas fa-cart-plus"></i>Add to
                    cart</button>
                </form>
              </div>
            </div>
          </div>
        </div>
       <?php } ?>
      </div>

    </div> <!-- card card-solid -->

  </section>
  <!-- Displaying Products End -->
</div> <!-- /.content-wrapper -->

<?php
  include "footer.php";
  $file = basename($_SERVER['PHP_SELF']);
  echo $file;
  include "scripts/script-$file";
?>  

