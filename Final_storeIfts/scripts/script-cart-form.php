<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- Ion Slider -->
<script src="plugins/ion-rangeslider/js/ion.rangeSlider.min.js"></script>
<!-- Bootstrap slider -->
<script src="plugins/bootstrap-slider/bootstrap-slider.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- Page specific script -->
<script src="scripts/script-probando.php"></script>
<script>
$(document).ready(function(){

$(".itemQty").on('change', function(){
    var $el= $(this).closest('tr');

    var pid= $el.find(".pid").val();
    var pprice= $el.find(".pprice").val();
    var qty= $el.find(".itemQty").val();

    location.reload(true);

    $.ajax({
        url:'action.php',
        method: 'post',
        cache: false,
        data: {
            qty: qty,
            pid:pid,
            pprice: pprice
        },
        success: function(response){
            console.log(qty);
            console.log(response);
        }
    })


});

load_cart_item_number();

function load_cart_item_number(){
    $.ajax({
        url: 'action.php',
        method: 'get',
        data: {cartItem: 'cart_item'},
        success: function(response){
        $("#cart-item").html(response);
        }

    });
}

});
</script>
</body>
</html>