<?php 
    include "header.php";
    $file = basename($_SERVER['PHP_SELF']);
    include "styles/style-$file";
?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Alta de productos</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Alta productos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- right column -->
          <div class="col-md-12">
           <!-- general form elements disabled -->
            <div class="card card-blue">
              <div class="card-header">
                <h3 class="card-title">Alta producto</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <form id="student" action="product.php" method="post" name="product-form">
                  <div class="row">
                    <div class="col-sm-4">
                      <div class="form-group">
                        <label>Nombre producto</label>
                        <input type="text" class="form-control" placeholder="nombre producto" name="pname">
                      </div>
                    </div>
                    <div class="col-sm-2">
                      <div class="form-group">
                        <label>Precio</label>
                        <input type="text" class="form-control" placeholder="precio producto" name="pprice">
                      </div>
                    </div>
                    <div class="col-sm-3">
                      <div class="form-group">
                        <label>Codigo de producto</label>
                        <input type="text" class="form-control" placeholder="codigo de producto" name="pcode">
                      </div>
                    </div>
                  </div>
                  
                    <div class="row">
                        <div class="col-md-6">
                        <div class="form-group">
                            <label>Imagen producto</label>
                            <input type="file" class="form-control" name="avatar" id="avatar">
                        </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <input type="hidden" name="role" value="2">
                        <input type="hidden" name="product-form" value="add">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                    </div>

                </form>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div > <!-- class="content-wrapper"--> 
<?php 
    include "footer.php";
    $file = basename($_SERVER['PHP_SELF']);
    include "scripts/script-$file";
?>
