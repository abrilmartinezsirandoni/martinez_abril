<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<!-- SweetAlert -->
<script src="dist/sweetalert2/sweetalert2.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script>
$(function(){
    var Toast= Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButtion: false,
        timer:3000
     });
    $('#course').validate({
        rules:{
            name: {
                required: true,
                minlength: 3,
            },
        },
        messages: {
            name:{
                required: "Este campo es obligatorio",
                minlength: "debe contener como minimo 3 caracteres"
            },

        },
        errorElement: 'span',
        errorPlacement: function (error, element){
            error.addClass( 'invalid-feedback');
            element.closest('.form-group').append(error);
        },
        highlight: function(element, errorClass, validClass){
            $(element).addClass('is-invalid');

        },
        unhighlight: function(element, errorClass, validClass){
            $(element).removeClass('is-invalid');
            $(element).addClass('is-invalid'); //pinta de verde si se ingresan la cant correcta de caracteres
        },
        submitHandler: function(form){
                var datos= $(form).serializeArray(); //capturamos datos del form y serializarlos para enviar a php
                $.ajax({
                    type: $(form).attr('method'), //que ajax capture en el atrbuto method del form
                    data: datos,  //les pasamos los datos
                    url: $(form).attr('action'), //la url va a ser el action
                    dataType: 'json',
                    success: function(data){  //data es lo que devuelve el script en la ult linea
                        console.log(data);
                        var resultado=data;  //lo guardamos en resultado
                        if (resultado.respuesta == 'exitoso'){
                            Toast.fire({
                                icon: 'success',
                                timer: 3000,
                                title: 'La carrera se guardo exitosamente con el id: '+ resultado.id+'!!'
                            })
                        }else if(resultado.respuesta == 'error' && resultado.errno===1062){
                            Toast.fire({
                                icon: 'error',
                                timer: 3000,
                                title: 'El id ya existe, cod de error: '+ resultado.errno
                            })
                        }else if(resultado.respuesta == 'error' && resultado.errno===1406){
                            Toast.fire({
                                icon: 'error',
                                timer: 3000,
                                title: 'Se excedio la cantidad de caracteres permitidos en la base '+ resultado.errno
                            })
                        }

                    }
                })

            }

    });

});
</script>