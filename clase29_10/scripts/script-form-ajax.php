<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<!-- SweetAlert2 -->
<link rel="stylesheet" href="plugins/sweetalert2/sweetalert2.min.css">

<script>
    $(document).ready(function(){
        $('#getUser').on('click', function(){
            /*nos interesa lo que ingresa el usuario, la info introducida en ese input*/
            var user_id= $('#user_id').val();
            $.ajax({
                //parametros que hay q declarle 
                type:'POST',      //se envian como encabezado de la petición, no estan expuestos a simple vista
                url:'getData.php', //a donde se envian esos valores
                dataType:'json',       /*tipo de dato a enviar*/
                data: {user_id:user_id},/*que vamvos a enviar nosotros*/ /*son dos objetos*/

                /*especificar q hacer en caso de envio y recepcion correctos */
                /*funcion de js que va a */
                success:function(data){
                    if(data.status =='ok'){
                        Swal.fire({
                            title:'usuario',
                            /*que el mismo modal nos impriam los datos del usuario*/

                            /*copiar cod form 

                             en cada value pasar
                             value= "${data.result.created}" readonly>   con el readonly no se puede modificar
                             */
                            html:`
                                <form id="quickForm">
                                    <div class="card-body">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nombre</label>
                                        <input type="text" name="Nombre" class="form-control" id="exampleInputText" placeholder="Enter first name" 
                                        value="${data.resul.nombre}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Apellido</label>
                                        <input type="text" name="Apeliido" class="form-control" id="exampleInputText" placeholder="Enter your last Name"
                                        value="${data.resul.apellido}" readonly>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Email address</label>
                                        <input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email"
                                        value="${data.resul.email}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Telefono</label>
                                        <input type="number" name="telefono" class="form-control" id="exampleInputNumber" placeholder="Telefono"
                                        value="${data.resul.telefono}" readonly>
                                    </div>
                                    <div class="form-group mb-0">
                                        <div class="custom-control custom-checkbox">
                                        <input type="checkbox" name="terms" class="custom-control-input" id="exampleCheck1">
                                        <label class="custom-control-label" for="exampleCheck1">I agree to the <a href="#">terms of service</a>.</label>
                                        </div>
                                    </div>
                                    </div>
                                    <!-- /.card-body -->
                                    <div class="card-footer">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </form>
                                
                                `,
                            icon: 'success',

                        });

                    }else{
                        Swal.fire({
                            title:'usuario',
                            text: 'El usuario no existe',
                            icon: 'error',
                            showConfirmButton: false, 
                            timer: 2000,

                        });
                    }
                }
            }); 
        
        });
    });
</script>