<?php

	// Add products into the cart table
	if (isset($_POST['pid'])) { //si la var esta definida 
        
        include_once 'config/db.php';

	  $pid = $_POST['pid']; //inicializa variables
	  $pname = $_POST['pname'];
	  $pprice = $_POST['pprice'];
	  $pimage = $_POST['pimage'];
	  $pcode = $_POST['pcode'];
	  $pqty = $_POST['pqty'];
	  $total_price = $pprice * $pqty;

	  $stmt = $conn->prepare('SELECT product_code FROM cart WHERE product_code=?');
	  $stmt->bind_param('s',$pcode);
	  $stmt->execute();
	  $res = $stmt->get_result();
	  $r = $res->fetch_assoc();
	  $code = $r['product_code']; //select code de la bbdd

        if (!$code) { // si no existe 
            $query = $conn->prepare('INSERT INTO cart (product_name,product_price,product_image,qty,total_price,product_code) VALUES (?,?,?,?,?,?)');
            $query->bind_param('ssssss',$pname,$pprice,$pimage,$pqty,$total_price,$pcode);
            $query->execute();

            echo '<div class="alert alert-success alert-dismissible mt-2">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <strong>Listo!</strong>
                            </div>';
        } else {
            echo '<div class="alert alert-danger alert-dismissible mt-2">
                            <button type="button" class="close" data-dismiss="alert"></button>
                            <strong>el producto ya fue agregado!</strong>
                            </div>';
        }
    } 
    	// Get no.of items available in the cart table
	if (isset($_GET['cartItem']) && isset($_GET['cartItem']) == 'cart_item') {
        include_once 'config/db.php';
        $stmt = $conn->prepare('SELECT * FROM cart');
        $stmt->execute();
        $stmt->store_result();
        $rows = $stmt->num_rows;
  
        echo $rows;
      }

    if(isset($_GET['remove'])){
        include_once 'config/db.php';
        $id= $_GET['remove'];
        $stmt= $conn->prepare("DELETE FROM cart WHERE id=?");
        $stmt-> bind_param("i", $id);
        $stmt->execute();

        $_SESSION['showAlert']= 'block';
        $_SESSION['message']= 'Se eliminó el producto del carrito!';
        header('location:cart-form.php');
    }
    if (isset($_GET['clear'])) {
        include_once 'config/db.php';
        $stmt = $conn->prepare('DELETE FROM cart');
        $stmt->execute();
        $_SESSION['showAlert'] = 'block';
        $_SESSION['message'] = 'Se eliminaron todos los productos del carrito!';
        header('location:cart-form.php');
    }
	// Set total price of the product in the cart table
	if (isset($_POST['qty'])) {
        include_once 'config/db.php';
        $qty = $_POST['qty'];
        $pid = $_POST['pid'];
        $pprice = $_POST['pprice'];
  
        $tprice = $qty * $pprice;
  
        $stmt = $conn->prepare('UPDATE cart SET qty=?, total_price=? WHERE id=?');
        $stmt->bind_param('isi',$qty,$tprice,$pid);
        $stmt->execute();
    }
  
      // Checkout and save customer info in the orders table
    if (isset($_POST['action']) && isset($_POST['action']) == 'order') {
        include_once 'config/db.php';
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $products = $_POST['products'];
        $grand_total = $_POST['grand_total'];
        $address = $_POST['address'];
        $pmode = $_POST['pmode'];
  
        $data = '';
  
        $stmt = $conn->prepare('INSERT INTO orders (name,email,phone,address,pmode,products,amount_paid)VALUES(?,?,?,?,?,?,?)');
        $stmt->bind_param('sssssss',$name,$email,$phone,$address,$pmode,$products,$grand_total);
        $stmt->execute();
        $stmt2 = $conn->prepare('DELETE FROM cart');
        $stmt2->execute();
        $data .= '<div class="text-center">
                                  <h1 class="display-4 mt-2 text-danger">Listo!</h1>
                                  <h2 class="text-success">Pedido realizado con exito</h2>
                                  <h4 class="bg-danger text-light rounded p-2">Productos : ' . $products . '</h4>
                                  <h4>Your Name : ' . $name . '</h4>
                                  <h4>Your Email : ' . $email . '</h4>
                                  <h4>Your Phone : ' . $phone . '</h4>
                                  <h4>Total a pagar : ' . number_format($grand_total,2) . '</h4>
                                  <h4>Payment Mode : ' . $pmode . '</h4>
                            </div>';
        echo $data;
      }
  ?>
