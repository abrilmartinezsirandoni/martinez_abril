 <?php
    include "header.php";
    $id = $_GET["id"];
    try {
        include_once 'config/db.php';
        $stmt = "SELECT * FROM products WHERE id=$id;";
        $perf = $conn->query($stmt);
    }   catch (Exception $e) {
        $error =$e->getMessage();
        echo $error;
    }
    $datos = $perf->fetch_assoc();

?>
  <!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Actualizar precio</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Modificacion productos</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
                <div class="card-header">
                  <h3 class="card-title"></h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <form id="id_product" action="product.php" name="product-form" method="post" > 
                      <div class="form-group">
                        <label> Actualizar valor del producto: <?php echo $datos["product_name"];?> </label>
                        <p> Valor actualmente registrado <?php echo $datos["product_price"];?></p>
                        <input type="text" name="name" class="form-control"  placeholder="Ingrese el nuevo precio">
                      </div>
                    

                      <div class="card-footer">
                        <input type= "hidden" name="id_product" value ="<?php echo $id; ?>">
                        <input type= "hidden" name="product-form" value="edit">
                        <button type= "submit" class="btn btn-primary">Guardar</button>
                      </div>
                    </form>
                </div>
            
            </div>
          </div>
            <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
<!-- /.content-wrapper -->

<?php
    include "footer.php";
        $file = basename($_SERVER['PHP_SELF']);
        echo $file;
        include "scripts/script-$file";
?>  

