<?php

if(isset($_POST['login-form'])) {
    $usuario = $_POST['email'];
    $password = $_POST['pass'];

    try {
        include_once 'config/db.php';
        $dat = $conn->prepare("SELECT * FROM users WHERE user_email = ?;");
        $dat->bind_param('s', $usuario);
        $dat->execute();
        $dat->bind_result($id, $userName, $email, $pass, $create_at, $update_at, $role);
        $errno = $dat->errno;
        $error = $dat->error;
        if($dat->affected_rows) {
            $existe = $dat->fetch();
            if($existe) {
                //if(password_verify($password, $pass)) {
                if ($password == $pass){   
                    session_start();
                    $_SESSION['user_id'] = $id;
                    $_SESSION['user_name'] = $userName;
                    $_SESSION['user_email'] = $email;
                    $_SESSION['role'] = $role;


                    $respuesta =array(
                        'respuesta' => 'exitoso',
                        'usuario' => $userName
                    );
                } else {
                    $respuesta =array(
                        'respuesta' => 'error_pass'
                    );
                }
            } else {
            $respuesta = array(
                'respuesta' => 'error',
                'email' => $usuario
            );
        }
    }

    } catch (Exception $e) {
        echo "Error: " . $e->getMessage();
    }

    die(json_encode($respuesta));
}