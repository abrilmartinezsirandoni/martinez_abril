<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- jquery-validation -->
<script src="plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="plugins/jquery-validation/additional-methods.min.js"></script>
<!-- Toastr -->
<script src="plugins/toastr/toastr.min.js"></script>
<!-- SweetAlert2 -->
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<script>
    $(function() {
        var Toast= Swal.mixin({
            toast: true,
            position: 'top-end',
            showConfirmButton: false,
            timer:3000
        });

            $('#id_product').validate({
                rules: {
                    name: {
                        required: true,
                        number: true,
                        minlength: 3,
                    },
                },
                messages: {
                    name: {
                        required: "Por favor ingrese un precio",
                        number: "Por favor ingrese un valor numerico",
                        minlength: "Debe contener como mínimo 3 caracteres",
                    },
                },
                errorElement: 'span',
                errorPlacement: function (error, element){
                    error.addClass( 'invalid-feedback');
                    element.closest('.input-group').append(error);
                },
                highlight: function(element, errorClass, validClass){
                    $(element).addClass('is-invalid');

                },
                unhighlight: function(element, errorClass, validClass){
                    $(element).removeClass('is-invalid');
                    $(element).addClass('is-valid');
                },
                submitHandler: function(form){
                    var datos= $(form).serializeArray();
                    $.ajax({
                        type: $(form).attr('method'),
                        data: datos,
                        url: $(form).attr('action'),
                        dataType: 'json',
                        success: function(data){
                            console.log(data);
                            var resultado=data;
                            if (resultado.respuesta == 'exitoso'){
                                Toast.fire({
                                    icon: 'success',
                                    timer: 3000,
                                    title: 'Se actualizo el precio del producto ' 
                                }).then(function (){
                                    window.location = 'index.php'
                                })
                            }else if(resultado.respuesta == 'error' && resultado.errno === 1062){
                                Toast.fire({
                                    icon: 'error',
                                    timer: 3000,
                                    title: 'El ID ya existe, código de error' + resultado.errno
                                })
                            }else if(resultado.respuesta == 'error' && resultado.errno === 1406){
                                Toast.fire({
                                    icon: 'error',
                                    timer: 3000,
                                    title: 'Se excedió la cantidad de caracteres permitodos en la base, código de error' + resultado.errno
                                })
                            }

                        }
                    });

                }
            });

    });
</script>
</body>
</html>