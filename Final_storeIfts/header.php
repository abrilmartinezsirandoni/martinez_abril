<?php 
  include 'config/sessions.php'; 
?>


<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1 , shrink-to-fit=no">
  <title>Store IFTS N°4</title>
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.min.css' />
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css' />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="plugins/summernote/summernote-bs4.min.css">

</head>
<body class="hold-transition sidebar-mini sidebar-collapse">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links --> <!-- ICONOS MODIFICADOS -->
    <ul class="navbar-nav ml-auto">

      <li class="nav-item">
        <a class="nav-link active" href="index.php"> <!-- "nav-link btn-xs btn-app" -->
            <i class="fas fa-barcode "></i> Products
        </a>
      </li>

      <li class="nav-item dropdown"> <!-- Productos-->
        <a class="nav-link active" href="checkout.php">
          <i class="fas fa-barcode "></i>Checkout
        </a>
      </li>

      <li class="nav-item"> <!-- CARRITO -->
        <a class="nav-link btn-xs btn-app" href="cart-form.php"> <!--AGREGADO HREF-->
          <i class="fas fa-shopping-cart"></i> Orders<span id="cart-item" class="badge badge-danger"></span><!--  CART-ITEM  VALDIADO EN AJAX -->  
        </a> <!-- CLASS SPAN OPCION  class bg-teal--> 
      </li>

      <!-- Messages Dropdown Menu --> <!-- CERRAR SESION -->
        <li class="nav-item dropdown">
          <a class="nav-link" href="login.php?cerrar_sesion=true"> <!--href="login.php?cerrar_sesion=true" -->
            <i class="fas fa-sign-out-alt text-danger"></i>
          </a>
        </li>

    </ul>
  </nav>
  <!-- /.navbar -->
  
  <!-- Main Sidebar Container --> <!-- BARRA OCULTA -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light"> Store IFTS</span>
    </a>

    <!-- Sidebar -->
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <!-- <div class="image">
          <img src="img/avatars< /?php echo $_SESSION['avatar']; ?>" class="img-circle elevation-2" alt="User Image">
        </div>-->
        <div class="info">
          <a href="#" class="d-block"><?php echo $_SESSION['user_name']; ?></a>
        </div>
      </div>

      <!-- SidebarSearch Form -->
      <div class="form-inline">
        <div class="input-group" data-widget="sidebar-search">
          <input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
          <div class="input-group-append">
            <button class="btn btn-sidebar">
              <i class="fas fa-search fa-fw"></i>
            </button>
          </div>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          <li class="nav-item menu-open">
            <a href="index.php" class="nav-link">
            <i class="fas fa-barcode "></i> 
              <p>
                Products
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="cart-form.php" class="nav-link ">
              <i class="fas fa-barcode "></i> 
              <p>
                Orders
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
          </li>
          <?php if ($_SESSION['role'] == '1') { ?>
            <li class="nav-item">
              <a href="index.php" class="nav-link">
                <i class="nav-icon fas fa-edit"></i>
                <p>
                  Products
                  <i class="fas fa-angle-left right"></i>
                </p>
              </a>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="product-form.php" class="nav-link">    <!-- SE AGREGO LINK A FORM.PHP -->
                    <i class="far fa-circle nav-icon"></i>
                    <p>Alta de productos</p>
                  </a>
                </li>
              </ul>
              <ul class="nav nav-treeview">
                <li class="nav-item">
                  <a href="list-modif-product.php" class="nav-link">    <!-- SE AGREGO LINK A FORM.PHP -->
                    <i class="far fa-circle nav-icon"></i>
                    <p>Modificacion productos</p>
                  </a>
                </li>
              </ul>
            </li>
            <?php } ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>


