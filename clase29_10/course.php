<?php

if($_POST['course-form'] == 'add'){

    $name= $_POST['name']; //guardar en var lo que venga en el param name
    try{
        include_once 'config/db.php';
        $stmt= $conn->prepare("INSERT INTO courses (course_name) VALUES (?)" );
        $stmt->bind_param('s', $name);
        $stmt->execute();

        $id_insertado= $stmt->insert_id; //la query responde con el id
        $errno= $stmt->errno; //error num que devuelve la bd 
        if($stmt->affected_rows && $errno===0){ //evalua que sea el mismo tipo de dato
            $respuesta = array(
                'respuesta'=> 'exitoso',
                'id'=> $id_insertado 

            );
            
        }elseif ($errno===1406) {
            $respuesta = array(
                'respuesta'=> 'error',
                'errno'=> $errno,
                'error'=> $stmt->error,

            );

        }elseif ($errno===1062) {
            $respuesta = array(
                'respuesta'=> 'error',
                'errno'=> $errno,
                'error'=> $stmt->error,

            );

        }
        $stmt->close();
        $conn->close();


    }catch (Exception $e){
        echo "Error: " . $e->getMessage();

    }
    die(json_encode($respuesta));

}




